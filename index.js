// console.log("Hello World!")

// ARITHMETIC OPERATORS

let x = 1397;
let y = 7831;

// let sum = 1397 + 7831
let sum = x + y;
console.log("Result of addition operator: " + sum);


let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x*y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

// modulus (%)
// Gets the remainder from 2 divided values.

let remainder = y % x;
console.log("Result of modulo operator: " + remainder);

// ASSIGNMENT OPERATOR (=)

// Basic Assignment
let assignmentNumber = 8;

assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

// shorthand method for assignment operator
assignmentNumber += 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

assignmentNumber -= 2;
console.log("Result of addition subtraction operator: " + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of addition multiplication operator: " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of addition division operator: " + assignmentNumber);

// Multiple operators and parenthesis

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

pemdas = (1 + (2 - 3)) * (4 / 5);
console.log("Result of second pemdas operation: " + pemdas);

// Incrementation cs Decrementation
// Incrementation (++)
// Decrementation (--)

let z = 1;

// ++z added 1 value to its original value
let increment = ++z;
console.log("Result of pre-incrementation operation: " + increment);
console.log("Result of pre-incrementation operation: " + z);

increment = z++;
console.log("Result of post-incrementation operation: " + increment);
console.log("Result of post-incrementation operation: " + z);

let	decrement = --z;
console.log("Result of pre-decrementation operation: " + decrement);
console.log("Result of pre-decrementation operation: " + z);

increment = z--;
console.log("Result of post-decrementation operation: " + decrement);
console.log("Result of post-decrementation operation: " + z);

// Type Coercion
let numA = "10";
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let nonCoercion = numC + numB;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// false = 0;
let numE = false + 1;
console.log(numE);

let numF = true + 1;
console.log(numF);

// Comparison Operators

let juan = "juan";

// Equality Operator (==)
// Checks 2 operands if they are equal/have the same content
// May return boolean value

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == "1");
console.log("juan" == "juan");
console.log("juan" == juan);

// Inequality Operator (!=)
// ! = not

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != "1");
console.log("juan" != "juan");
console.log("juan" != juan);
console.log(0 != false);

// Strict Equality Operator (===)
// Compares the content and also the data type
console.log(1 === 1);
console.log(1 === 2);
console.log("juan" === juan);

// Strict Inequality Operator (!==)
console.log(1 !== 1);
console.log(1 !== 2);
console.log("juan" !== juan);

// Relational Operator
let a = 50;
let b = 65;

// GT (>) Greater than operator
let isGreaterThan = a > b;
console.log(isGreaterThan);
// LT (<) Less Than operator
let isLessThan = a < b;
console.log(isLessThan);

// GTE (>=) 
let isGTorEqual = a > b;
console.log(isGTorEqual);
// GTE (<=) 
let isLTorEqual = a < b;
console.log(isLTorEqual);

let numStr = "30";
console.log(a > numStr);

let str = "twenty";
console.log(b >= str);
// In some events, we can receive NaN
// NaN == Not a Number


// Logical Operators
let isLegalAge = true;
let isRegistered = false;

// Logical AND Operator (&& -- Ampersands)
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + allRequirementsMet);

// Logical OR Operator (|| -- Double Pipe)
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet);

// Loginal NOT Operator(! -- Exclamation Point)
// Returns Opposit value

let someRequirementsNotMet = !isRegistered;
console.log("Result of logical NOT operator: " + someRequirementsNotMet);











